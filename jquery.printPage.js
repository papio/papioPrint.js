/**
 * jQuery 页面打印插件
 * 
 */

(function( $ ){

  $.fn.printPage = function(options) {
    var pluginOptions = {
      attr : "href",
      url : false
    };

    $.extend(pluginOptions, options);

    this.live("click", function(){
    	PrintPgae.loadPrintDocument(this, pluginOptions);
    	return false;
    });
	
	var PrintPgae={
		loadPrintDocument:function(el, pluginOptions){
            PrintPgae.addIframeToPage(el, pluginOptions);
		},
		iframe: function(url){
	        return '<iframe id="printPage" name="printPage" src='+url+' style="position:absolute;top:0px; left:0px;width:0px; height:0px;border:0px;overfow:none; z-index:-1"></iframe>';
	    },
		addIframeToPage:function(el, pluginOptions){
			var url=pluginOptions.url?pluginOptions.url:$(el).attr(pluginOptions.attr);
			if(!$("#printPage")[0]){
				$("body").append(PrintPgae.iframe(url));
				$('#printPage').one("load",function() {
					PrintPgae.printit(pluginOptions);
				});
			}else{
				$('#printPage').attr("src", url);
                $('#printPage').one("load",function() {
                    PrintPgae.printit(pluginOptions);
                });
			}
		},
		//打印
		printit:function(pluginOptions){
            var LODOP=getLodop();
            LODOP.PRINT_INIT(pluginOptions.strTaskName);
            LODOP.SET_PRINT_PAGESIZE(pluginOptions.intOrient,0,0,pluginOptions.strPageName);
            LODOP.SET_SHOW_MODE("LANDSCAPE_DEFROTATED",1);
            LODOP.ADD_PRINT_TABLE(30,30,"95%","95%",document.getElementById("printPage").contentWindow.document.getElementsByTagName('html')[0].innerHTML);
            LODOP.PREVIEW();
	    }
	}
	
  };
})( jQuery );